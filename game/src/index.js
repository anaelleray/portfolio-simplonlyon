//DECLARATIONS DES VARIABLES!!
let joueur = document.querySelector('.joueur')

let ghost = document.querySelector(".ghost")
let ghost2 = document.querySelector(".ghost2")
let ghost3 = document.querySelector(".ghost3")
let ghost4 = document.querySelector(".ghost4")
let ghost5 = document.querySelector(".ghost5")
let ghost6 = document.querySelector(".ghost6")
let ghost7 = document.querySelector(".ghost7")
let ghost8 = document.querySelector(".ghost8")
let ghost9 = document.querySelector(".ghost9")

let goal = document.querySelector('.diamant')

let clicked = false;

//MOUVEMENT DU JOUEUR : TRACKER DE LA SOURIS

joueur.addEventListener('mousedown', function () {
    clicked = true;
});

window.addEventListener('mouseup', function () {
    clicked = false;
});

window.addEventListener('mousemove', function (event) {


    if (clicked) {

        joueur.style.top = event.clientY + 'px';
        joueur.style.left = event.clientX + 'px';
    }

});

//FONCTION ALERTE COLLISIONS FANTOMES:
function alertFail() {
    let divFail = document.createElement('div');
    divFail.classList.add('loser');
    document.body.appendChild(divFail);
    divFail.textContent = "Tu as perdu ? Mais c'est un jeu simple pourtant...";
    divFail.addEventListener('click', function () {
        document.location.reload(true);
    });
}

//FONCTION ALERTE COLLISIONS DIAMANT
function alertWin(){
    let divWin = document.createElement('div');
    divWin.classList.add('winner');
    document.body.appendChild(divWin);
    divWin.textContent = "Super, tu as récupéré le diamant ! Toutefois le vol c'est mal tu devrais avoir honte..."
    divWin.addEventListener('click', function(){
        document.location.reload(true);
    });
}

//VERIFICATION DES COLLISIONS AVCE LES FANTOMES !! (X9)

setInterval(function () {
    if (joueur.offsetLeft < ghost.offsetLeft + ghost.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost.offsetLeft &&
        joueur.offsetTop < ghost.offsetTop + ghost.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost2.offsetLeft + ghost2.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost2.offsetLeft &&
        joueur.offsetTop < ghost2.offsetTop + ghost2.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost2.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost3.offsetLeft + ghost3.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost3.offsetLeft &&
        joueur.offsetTop < ghost3.offsetTop + ghost3.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost3.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost4.offsetLeft + ghost4.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost4.offsetLeft &&
        joueur.offsetTop < ghost4.offsetTop + ghost4.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost4.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost5.offsetLeft + ghost5.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost5.offsetLeft &&
        joueur.offsetTop < ghost5.offsetTop + ghost5.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost5.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost6.offsetLeft + ghost6.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost6.offsetLeft &&
        joueur.offsetTop < ghost6.offsetTop + ghost6.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost6.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost7.offsetLeft + ghost7.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost7.offsetLeft &&
        joueur.offsetTop < ghost7.offsetTop + ghost7.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost7.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost8.offsetLeft + ghost8.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost8.offsetLeft &&
        joueur.offsetTop < ghost8.offsetTop + ghost8.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost8.offsetTop) {

            alertFail()
    }

}, 50)

setInterval(function () {
    if (joueur.offsetLeft < ghost9.offsetLeft + ghost9.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > ghost9.offsetLeft &&
        joueur.offsetTop < ghost9.offsetTop + ghost9.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > ghost9.offsetTop) {

            alertFail()
    }

}, 50)

// COLLISION AVEC LE DIAMANT

setInterval(function () {
    if (joueur.offsetLeft < goal.offsetLeft + goal.offsetWidth &&
        joueur.offsetLeft + joueur.offsetWidth > goal.offsetLeft &&
        joueur.offsetTop < goal.offsetTop + goal.offsetHeight &&
        joueur.offsetHeight + joueur.offsetTop > goal.offsetTop) {

        alertWin()

    }

}, 70)

//HOW TO DO LA REGLE DE L'ENFER POUR LES COLISIONS
//if (rect1.x < rect2.x + rect2.width &&
//    rect1.x + rect1.width > rect2.x &&
//    rect1.y < rect2.y + rect2.height &&
//    rect1.height + rect1.y > rect2.y) {
//     // collision détectée !
// }
