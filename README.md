


<h2>Users stories :

1) Comme un **recruteur** je veux pouvoir avoir une vison claire des projects et des compétences pour estimer le potetiel.

2) Comme un **formateur**, je souhaite pouvoir identifier clairement le portfolio pour estimer les points forts/faibles et les différentes révisions à faire.

3) Comme une **start-up** je souhaite avoir accès à des projets pour déterminer la pertinance du contenu en vue d'une embauche/collab.

4) Comme un **futur/nouveau Simplonien(nes)** je souhaite avoir accès à des portfolios inspirant témoigant de la personallité de l'auteur et ces compétences.



<h2>Wireframe</h2>

![maquette porto 1](PortfolioWireframe.png)